﻿using System.Collections;

using System.Collections.Generic;

using UnityEngine;



public class BeeSpawner : MonoBehaviour {

    public int nBees = 5;
    public BeeMove beePrefab;
    public float xMin, yMin;
    public float width, height;
	public float minSpawnTime = 1.0f;
	public float maxSpawnTime = 3.0f;
	private float beeCountdown = 0.0f;

void Start() {
	for (int i = 0; i < nBees; i++) {
    	// instantiate a bee
		BeeMove bee = Instantiate (beePrefab);
		// attach to this object in the hierarchy
		bee.transform.parent = transform;            
		// give the bee a name and number
		bee.gameObject.name = "Bee " + i;
		// move the bee to a random position within 
		// the bounding rectangle
		float x = xMin + Random.value * width;
		float y = yMin + Random.value * height;
		bee.transform.position = new Vector2 (x, y);
	}
}

void Update() {
		beeCountdown -= Time.deltaTime;
		if (beeCountdown <= 0.0f) {
			BeeMove bee = Instantiate (beePrefab);
			// attach to this object in the hierarchy
			bee.transform.parent = transform;            
			// give the bee a name and number
			bee.gameObject.name = "Bee " + Random.value;
			// move the bee to a random position within 
			// the bounding rectangle
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
			beeCountdown = Random.Range (minSpawnTime, maxSpawnTime);
		}
}


public void DestroyBees(Vector2 centre, float radius) {
	// destroy all bees within ‘radius’ of ‘centre’
	for (int i = 0; i < transform.childCount; i++) {
		Transform child = transform.GetChild(i);
		Vector2 v = (Vector2)child.position - centre;
		if (v.magnitude <= radius) {
			Destroy(child.gameObject);
		}
	}
}

}